package com.polahniuk.queue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Queue;

public class TestQueue {

    private static Logger log = LogManager.getLogger(TestQueue.class);

    public static void main(String[] args) {
        Deque<String> deque = new DequeImpl<>();
        System.out.println(deque);
        deque.offerFirst("1");
        deque.offerFirst("2");
        deque.offerFirst("3");
        deque.offerLast("4");
        deque.offerLast("5");
        deque.offerLast("6");
        log.debug("PeekFirst " + deque.peekFirst());
        log.debug("PeekLast " + deque.peekLast());
        log.debug("PollFirst " + deque.pollFirst());
        log.debug("PollLast " + deque.pollLast());
        log.debug("PeekLast " + deque.peekLast());

        //-------------------------------------------------------
        Queue<Integer> queue = new PriorityQueueImpl();
        queue.offer(1);
        queue.offer(2);
        queue.offer(3);
        log.debug("Peek " + queue.peek());
        log.debug("Poll " + queue.poll());
        log.debug("Poll " + queue.poll());
        log.debug("Peek " + queue.peek());
    }
}
