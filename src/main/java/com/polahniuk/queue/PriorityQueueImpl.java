package com.polahniuk.queue;

import java.util.AbstractQueue;
import java.util.Arrays;
import java.util.Iterator;

public class PriorityQueueImpl<E> extends AbstractQueue<E> {

    private static final int DEFAULT_SIZE = 5;
    private Object[] priorityQueue;
    private int size = 0;

    public PriorityQueueImpl() {
        priorityQueue = new Object[DEFAULT_SIZE];
    }

    public PriorityQueueImpl(int size) {
        priorityQueue = new Object[size];
    }

    public PriorityQueueImpl(PriorityQueueImpl<? extends E> priorityQueue) {
        this.priorityQueue = priorityQueue.toArray();
        this.size = priorityQueue.size();
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(priorityQueue, size);
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public int size() {
        return size;
    }

    private void grow() {
        int oldSize = priorityQueue.length;
        int newSize = oldSize + 32;
        priorityQueue = Arrays.copyOf(priorityQueue, newSize);
    }

    @Override
    public boolean offer(Object o) {
        if (o == null) {
            throw new NullPointerException();
        }
        if (priorityQueue.length == size) {
            grow();
        }
        priorityQueue[size] = o;
        size++;
        return true;
    }

    @Override
    public E poll() {
        if (size == 0) {
            return null;
        }
        E o = (E) priorityQueue[0];
        System.arraycopy(priorityQueue, 1, priorityQueue, 0, priorityQueue.length - 1);
        size--;
        return o;
    }

    @Override
    public E peek() {
        if (size == 0) {
            return null;
        }
        E o = (E) priorityQueue[0];
        return o;
    }
}
