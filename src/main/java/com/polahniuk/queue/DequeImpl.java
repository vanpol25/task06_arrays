package com.polahniuk.queue;

import java.util.*;

public class DequeImpl<E> implements Deque{

    private static final int DEFAULT_SIZE = 5;
    private Object[] deque;
    private int size = 0;

    public DequeImpl() {
        deque = new Object[DEFAULT_SIZE];
    }

    public DequeImpl(int size) {
        deque = new Object[size];
    }

    public DequeImpl(PriorityQueueImpl<? extends E> priorityQueue) {
        this.deque = priorityQueue.toArray();
        this.size = priorityQueue.size();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean offerFirst(Object o) {
        if (o == null) {
            throw new NullPointerException();
        }
        System.arraycopy(deque, 0, deque, 1, deque.length - 1);
        deque[0] = o;
        size++;
        return true;
    }

    @Override
    public boolean offerLast(Object o) {
        if (o == null) {
            throw new NullPointerException();
        }
        if (deque.length == size) {
            grow();
        }
        deque[size++] = o;
        return true;
    }

    @Override
    public E pollFirst() {
        if (size == 0) {
            return null;
        }
        E o = (E) deque[0];
        System.arraycopy(deque, 1, deque, 0, deque.length - 1);
        size--;
        return o;
    }

    @Override
    public E pollLast() {
        if (size == 0) {
            return null;
        }
        E o = (E) deque[size-1];
        size--;
        return o;
    }

    @Override
    public E peekFirst() {
        if (size == 0) {
            return null;
        }
        E o = (E) deque[0];
        return o;
    }

    @Override
    public Object peekLast() {
        if (size == 0) {
            return null;
        }
        E o = (E) deque[size - 1];
        return o;
    }

    @Override
    public String toString() {
        return "DequeImpl{" +
                "deque=" + Arrays.toString(deque) +
                ", size=" + size +
                '}';
    }

    private void grow() {
        int oldSize = deque.length;
        int newSize = oldSize + 32;
        deque = Arrays.copyOf(deque, newSize);
    }

    //---------------------------------------------------------------------------

    @Override
    public Object[] toArray() {
        return null;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return null;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Iterator descendingIterator() {
        return null;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void addFirst(Object o) {

    }

    @Override
    public void addLast(Object o) {

    }

    @Override
    public Object removeFirst() {
        return null;
    }

    @Override
    public Object removeLast() {
        return null;
    }

    @Override
    public Object getFirst() {
        return null;
    }

    @Override
    public Object getLast() {
        return null;
    }

    @Override
    public boolean removeFirstOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean add(Object o) {
        return true;
    }

    @Override
    public boolean offer(Object o) {
        return false;
    }

    @Override
    public Object remove() {
        return null;
    }

    @Override
    public Object poll() {
        return null;
    }

    @Override
    public Object element() {
        return null;
    }

    @Override
    public Object peek() {
        return null;
    }

    @Override
    public void push(Object o) {

    }

    @Override
    public Object pop() {
        return null;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }
}
