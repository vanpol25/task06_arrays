package com.polahniuk.generic;

import java.util.ArrayList;
import java.util.List;


/**
 * Generic class – container with wildcards. You can put and get units from container.
 * @version 1
 * @author Ivan Polahniuk
 */
public class GenericWithWildcards<T> {

    private List<? extends T> list = new ArrayList<>();

    public T getList(int index) {
        return list.get(index);
    }

    public void putList(List<? extends T> list) {
        this.list = list;
    }

}
