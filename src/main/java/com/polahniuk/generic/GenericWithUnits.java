package com.polahniuk.generic;

import java.util.ArrayList;
import java.util.List;

/**
 * Generic class – container with units. You can put and get units from container.
 * @version 1
 * @author Ivan Polahniuk
 */
public class GenericWithUnits<T> {

    private List<T> list = new ArrayList<>();

    public T getList(int index) {
        return list.get(index);
    }

    public void putList(T t) {
        list.add(t);
    }
}
