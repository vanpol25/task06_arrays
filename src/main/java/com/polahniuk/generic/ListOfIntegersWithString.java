package com.polahniuk.generic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This class has method {@link ListOfIntegersWithString#addStringToListOfIntegers(String)}.
 * This method can add String to the list of integers {@link ListOfIntegersWithString#integerList}.
 * @version 1
 * @author Ivan Polahniuk
 */
public class ListOfIntegersWithString {

    private List<Integer> integerList = new ArrayList<>(Arrays.asList(1, 3));

    public List<Integer> addStringToListOfIntegers(String string) {
        List list = new ArrayList();
        list.add(string);
        integerList.addAll(list);
        return integerList;
    }
}
