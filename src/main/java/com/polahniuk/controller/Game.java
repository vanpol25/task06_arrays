package com.polahniuk.controller;

import com.polahniuk.model.*;
import org.apache.logging.log4j.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * This class create objects for a game.
 * As it is a controller. This class works with view {@link com.polahniuk.view.Hall}
 * and with modal part.
 *
 * @author Ivan Polahniuk
 * @version 1
 */
public class Game {

    private Logger log = LogManager.getLogger(Game.class);

    private static Game game;

    private static final Random RANDOM = new Random();
    private static final int DOORS_QUANTITY = 10;

    private List<Door> doors;
    private Hero hero;

    private Game() {
        log.debug("creating Game");
        createGame();
    }

    public static Game getGame() {
        if (game == null) {
            game = new Game();
        }
        return game;
    }

    private void createGame() {
        hero = Hero.getHero();
        doors = new ArrayList<>();
        for (int i = 0; i < DOORS_QUANTITY; i++) {
            doors.add(
                    new Door(createObjectBehindTheDoor())
            );
        }
    }

    private BehindTheDoor createObjectBehindTheDoor() {
        BehindTheDoor behindTheDoor;
        if (RANDOM.nextBoolean()) {
            behindTheDoor = new MagicArtifact();
        } else {
            behindTheDoor = new Monster();
        }
        return behindTheDoor;
    }

    public List<Door> getDoors() {
        return doors;
    }

    public Hero getHero() {
        return hero;
    }

    public int getNumberOfDeath(int index) {
        int numberOfDeath;
        if (index == 0) {
            return 0;
        }
        if (doors.get(index - 1).isClosed() &&
                doors.get(index - 1).getBehindTheDoor().getName().equals("Monster") &&
                doors.get(index - 1).getBehindTheDoor().getPower() > hero.getPower()) {
            numberOfDeath = 1;
        } else {
            numberOfDeath = 0;
        }
        numberOfDeath += getNumberOfDeath(index - 1);
        return numberOfDeath;
    }
}
