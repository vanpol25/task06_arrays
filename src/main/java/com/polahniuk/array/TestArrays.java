package com.polahniuk.array;

import com.polahniuk.generic.GenericWithUnits;
import com.polahniuk.generic.GenericWithWildcards;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * This is the test class for array tasks.
 *
 * @author Ivan Polahniuk
 * @version 1
 */
public class TestArrays {

    private static Logger log = LogManager.getLogger(TestArrays.class);

    public static void main(String[] args) {
        List<Number> numbers = new ArrayList<>();
        List<Integer> integers = new ArrayList<>();

        GenericWithWildcards<Number> generic1 = new GenericWithWildcards<>();
        GenericWithUnits<Number> generic2 = new GenericWithUnits<>();

        generic1.putList(numbers);
        generic1.putList(integers);

        generic1.putList(numbers);

        //-------------------------------------
        String s = "";
        long start1 = System.currentTimeMillis();
        ContainerString list1 = new ContainerString();
        for (int i = 0; i < 10000; i++) {
            list1.add("new String");
        }
        for (int i = 0; i < 10000; i++) {
            s += list1.get(i);
        }
        long end1 = System.currentTimeMillis();
        s = "";
        long start2 = System.currentTimeMillis();
        ArrayList<String> list2 = new ArrayList<>();
        for (int i = 0; i < 10000; i++) {
            list2.add("new String");
        }
        for (int i = 0; i < 10000; i++) {
            s += list2.get(i);
        }
        long end2 = System.currentTimeMillis();

        long res1 = end1 - start1;
        long res2 = end2 - start2;
        log.info("ContainerString speed = " + res1);
        log.info("ArrayList speed = " + res2);

    }

    public static void test(List<? extends Number> numbers) {
        for (Number n : numbers) {
            log.debug(n);
        }
    }
}
