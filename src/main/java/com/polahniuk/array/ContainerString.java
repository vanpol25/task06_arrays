package com.polahniuk.array;

import java.util.Arrays;

/**
 * Its a container for String.
 * Has put {@link ContainerString#add} method.
 * And get {@link ContainerString#get} method.
 *
 * @version 1
 * @author Ivan Polahniuk
 */
public class ContainerString {

    private static final int DEFAULT_SIZE = 10;
    private String[] arrString;
    private int size = 0;

    public ContainerString() {
        arrString = new String[DEFAULT_SIZE];
    }

    public int size() {
        return size;
    }

    public String get(int index) {
        if (size == 0 || index >= size) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return arrString[index];
    }

    public void add(String string) {
        if (size == arrString.length) {
            grow();
        }
        arrString[size] = string;
        size++;
    }

    private void grow() {
        int newCapacity = arrString.length * 2;
        arrString = Arrays.copyOf(arrString, newCapacity);
    }
}
