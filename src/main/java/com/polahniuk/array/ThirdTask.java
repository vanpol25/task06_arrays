package com.polahniuk.array;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This logical task is implemented in this class.
 * Here is the array {@link ThirdTask#list}.
 * I remove all elements that have repeated sequence except one element.
 *
 * @author Ivan Polahniuk
 * @version 1
 */
public class ThirdTask {

    private List<Integer> list = new ArrayList<>(Arrays.asList(1, 1, 2, 2, 3, 4, 1, 2, 4, 4, 4, 5, 6, 3, 7, 8, 9, 9, 9, 9, 1, 2, 3, 3, 4));

    public List<Integer> removeRepeatedFromSequences() {
        List<Integer> newList = list;
        Integer next;
        for (int i = 0; i < newList.size(); i++) {
            if (newList.size() > i + 1) {
                next = newList.get(i + 1);
                if (newList.get(i).equals(next)) {
                    newList.remove(i + 1);
                    i--;
                }
            }
        }
        return newList;
    }
}