package com.polahniuk.array;

import java.util.*;

/**
 * This logical task is implemented in this class.
 * Here is the array {@link SecondTask#list}.
 * I remove all elements that have repeating more than two times.
 *
 * @version 1
 * @author Ivan Polahniuk
 */
public class SecondTask {

    private List<Integer> list = new ArrayList<>(Arrays.asList(1, 1, 1, 2, 3, 4, 5, 5, 5, 6, 6, 9, 9, 9, 9));

    private Object toRemove;
    private int counter;

    public List<Integer> removeMassRepeating() {
        List<Integer> newList = list;
        Set<Integer> newSet = new HashSet<>(list);
        for (Integer i : newSet) {
            toRemove = i;
            counter = 0;
            for (Integer n : newList) {
                if (i.equals(n)) {
                    counter++;
                }
            }
            if (counter > 2) {
                while (newList.contains(toRemove)) {
                    newList.remove(toRemove);
                }
            }
        }
        return newList;
    }

}
