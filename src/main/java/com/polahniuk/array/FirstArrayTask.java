package com.polahniuk.array;

import java.util.*;

/**
 * This logical task is implemented in this class.
 * Here is to arrays {@link FirstArrayTask#firstArray} and {@link FirstArrayTask#secondArray}.
 * I create the third array which can has:
 * 1) Elements that repeated in first and second arrays.
 * 2) Elements that are unique from first array.
 *
 * @version 1
 * @author Ivan Polahniuk
 */
public class FirstArrayTask {

    private List<Integer> firstArray = Arrays.asList(1, 2, 3, 4, 5);
    private List<Integer> secondArray = Arrays.asList(3, 4, 5, 6, 7);
    private List<Integer> array;

    public List<Integer> createRepeated() {
        array = new ArrayList<>();
        for (int i = 0; i < firstArray.size(); i++) {
            for (int j = 0; j < secondArray.size(); j++) {
                if (firstArray.get(i).equals(secondArray.get(j))) {
                    array.add(firstArray.get(i));
                }
            }
        }
        return new ArrayList<>(new HashSet<>(array));
    }

    public List<Integer> createUniqueFromFirst() {
        array = new ArrayList<>(new HashSet<>(firstArray));
        for (Integer n : secondArray) {
            if (array.contains(n)) {
                array.remove(n);
            }
        }
        return array;
    }

}
