package com.polahniuk.view;

import com.polahniuk.controller.Game;
import com.polahniuk.model.BehindTheDoor;
import com.polahniuk.model.Door;

import java.util.List;
import java.util.Scanner;

/**
 * It is view. Works with the console.
 * Has Scanner {@link Scanner} to communicate with player.
 *
 * @author Ivan Polahniuk
 * @version 1
 */
public class Hall {

    private static Hall hall;
    private Game game = Game.getGame();
    private List<Door> doors = game.getDoors();
    private boolean process = true;
    private Scanner sc = new Scanner(System.in);

    public static Hall getHall() {
        if (hall == null) {
            hall = new Hall();
        }
        return hall;
    }

    public void start() {
        init();
    }

    private void init() {
        BehindTheDoor behindTheDoor;
        System.out.println("Hello Hero!\n" +
                "In front of you 10 doors, there is can by magic artifact which gives you from 10 till 80 powers.\n" +
                "Or a monster which wants blood and will fight with you. Its has from 5 till 100 power.\n" +
                "The winner will be person with more power.\n" +
                "You have " + showHeroHealth() + ".\n" +
                "Kill all monsters to win!\n");
        System.out.println("Behind the door ");
        do {
            showDoors();
            System.out.println("Now you can die behind " + game.getNumberOfDeath(doors.size()) + " doors.");
            System.out.println("Which the door you want to open?");
            behindTheDoor = openDoorNumber();
            behindTheDoor(behindTheDoor);
        } while (process);
    }

    private void behindTheDoor(BehindTheDoor behindTheDoor) {
        String nameBehindTheDoor = behindTheDoor.getName();
        System.out.println("There was " + nameBehindTheDoor + ".\n" +
                "with " + behindTheDoor.getPower() + " power.");
        boolean isAliveHero = behindTheDoor.heroPowerOperate(game.getHero());
        showResOfDoorOpening(isAliveHero);
    }

    private BehindTheDoor openDoorNumber() {
        int doorNumber;
        BehindTheDoor behindTheDoor = null;
        do {
            try {
                doorNumber = sc.nextInt();
                behindTheDoor = doors.get(doorNumber - 1).open();
            } catch (IndexOutOfBoundsException e) {
                System.err.println("There is no such door! Choose another.");
            } catch (Exception e) {
                System.err.println("This door is opened! Choose another.");
            }
        } while (behindTheDoor == null);
        return behindTheDoor;
    }

    private void showResOfDoorOpening(boolean bool) {
        if (bool) {
            System.out.println("Congratulations! You can play more!\n" +
                    "You have " + showHeroHealth() + ".\n");
        } else {
            System.out.println("You lose!");
            endGame();
        }
    }

    private String showHeroHealth() {
        return game.getHero().getPower() + " power";
    }

    private void showDoors() {
        String leftAlignFormat = "| %-10s | %-9s |%n";
        String door;
        System.out.format("+-------------+-----------+%n");
        System.out.format("| Door №     | Сondition |%n");
        System.out.format("+-------------+-----------+%n");
        for (int i = 0; i < doors.size(); i++) {
            if (doors.get(i).isClosed()) {
                door = "closed";
            } else {
                door = "opened";
            }
            System.out.format(leftAlignFormat, "Door №" + (i + 1), door);
        }
        System.out.format("+-------------+-----------+%n");
    }

    private void endGame() {
        process = false;
    }
}
