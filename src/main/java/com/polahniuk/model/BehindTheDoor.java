package com.polahniuk.model;

/**
 * Interface has methods described in {@link MagicArtifact} and {@link Monster}.
 *
 * @author Ivan Polahniuk
 * @version 1
 */
public interface BehindTheDoor {
    boolean heroPowerOperate(Hero hero);

    String getName();

    int getPower();
}
