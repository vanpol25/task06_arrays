package com.polahniuk.model;

import java.util.Random;

/**
 * Class describes monster that can be behind the door.
 * Can kill the hero {@link Hero}.
 *
 * @author Ivan Polahniuk
 * @version 1
 */
public class Monster implements BehindTheDoor {

    private static final int DEFAULT_MIN_POWER = 5;
    private static final int DEFAULT_MAX_POWER = 100;
    private static final String DEFAULT_NAME = "Monster";

    private String name;
    private int power;

    public Monster() {
        this.name = DEFAULT_NAME;
        this.power = new Random().nextInt(DEFAULT_MAX_POWER - DEFAULT_MIN_POWER + 1) + DEFAULT_MIN_POWER;
    }

    @Override
    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    @Override
    public boolean heroPowerOperate(Hero hero) {
        boolean isAliveHero;
        if (hero.getPower() >= getPower()) {
            isAliveHero = true;
        } else {
            isAliveHero = false;
        }
        return isAliveHero;
    }
}
