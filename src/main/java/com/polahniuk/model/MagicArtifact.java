package com.polahniuk.model;

import java.util.Random;

/**
 * Class describes magic artifact that can be behind the door.
 * Can gives more power for {@link Hero}
 *
 * @author Ivan Polahniuk
 * @version 1
 */
public class MagicArtifact implements BehindTheDoor {

    private static final int MIN_BONUS_VALUE = 10;
    private static final int MAX_BONUS_VALUE = 80;
    private static final String DEFAULT_NAME = "Magic Artifact";

    private String name;
    private int power;

    public MagicArtifact() {
        name = DEFAULT_NAME;
        power = new Random().nextInt(MAX_BONUS_VALUE - MIN_BONUS_VALUE + 1) + MIN_BONUS_VALUE;
    }

    @Override
    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    @Override
    public boolean heroPowerOperate(Hero hero) {
        hero.setPower(hero.getPower() + getPower());
        return true;
    }
}
