package com.polahniuk.model;

/**
 * Class describes the main hero.
 * This class is a singleton.
 *
 * @author Ivan Polahniuk
 * @version 1
 */
public class Hero {

    private static Hero hero;
    private static final int DEFAULT_POWER = 25;
    private static final String DEFAULT_NAME = "Hero";

    private String name;
    private int power;

    private Hero(String name) {
        this.name = name;
        this.power = DEFAULT_POWER;
    }

    public static Hero getHero() {
        if (hero == null) {
            hero = new Hero(DEFAULT_NAME);
        }
        return hero;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }
}
