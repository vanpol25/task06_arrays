package com.polahniuk.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Class Door has methods to operate his fields.
 * Has object {@link BehindTheDoor}.
 * And switcher is it closed or opened {@link Door#closed}
 *
 * @author Ivan Polahniuk
 * @version 1
 */
public class Door {

    private Logger log = LogManager.getLogger(Door.class);

    private BehindTheDoor behindTheDoor;
    private boolean closed = true;

    public Door(BehindTheDoor behindTheDoor) {
        this.behindTheDoor = behindTheDoor;
        log.debug("Creating Door object");
    }

    public void setBehindTheDoor(BehindTheDoor behindTheDoor) {
        this.behindTheDoor = behindTheDoor;
    }

    public BehindTheDoor getBehindTheDoor() {
        return behindTheDoor;
    }

    public boolean isClosed() {
        return closed;
    }

    public BehindTheDoor open() throws Exception {
        BehindTheDoor person;
        if (isClosed()) {
            person = behindTheDoor;
            closed = false;
        } else {
            person = null;
            log.info("Is already opened");
            throw new Exception("Is already opened");
        }
        return person;
    }

    @Override
    public String toString() {
        return "Door{" +
                "behindTheDoor=" + behindTheDoor.getName() +
                ", power" + behindTheDoor.getPower() +
                ", closed=" + closed +
                '}';
    }
}
