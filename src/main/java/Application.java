import com.polahniuk.view.Hall;

/**
 * The main class with entry point {@link Application#main(String[])}.
 * Create object {@link Hall} which starts the game.
 *
 * @author Ivan Polahniuk
 * @version 1
 */
public class Application {
    public static void main(String[] args) {
        Hall hall = Hall.getHall();
        hall.start();
    }
}
